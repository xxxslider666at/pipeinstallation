import {forwardRef, Inject, Injectable} from "@angular/core";
import { Router } from "@angular/router";
import { IDriver } from "./idriver";
import { ICore } from "./core/icore";
import { IEngine } from "./engine/iengine";
import { Core } from "./core/core";
import { Engine } from "./engine/engine";
import { IModel } from "./model/imodel";
import { Model } from "./model/model";
import { Authentication } from "./model/authentication/authentication";

@Injectable()

export class Driver implements IDriver {

    constructor(public router: Router,  @Inject(forwardRef(() => Authentication)) public authentication: Authentication) {
        this._model = new Model(this);

        this._core = new Core(this);
        this._engine = new Engine(this, this._model, authentication);
        this._router = router;
    }
    _model: IModel;

    _core: ICore;
    _engine: IEngine;
    _router: Router;
}
