import {User} from "../../model/user/user";

export interface IAuthenticationService {
    _runAuthorization(user: User): boolean;
    _stopAuthorization(): void;
}
