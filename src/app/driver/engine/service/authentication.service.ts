import { Injectable } from '@angular/core';
import { IAuthenticationService } from "./iauthentication-service";
import { Authentication } from '../../model/authentication/authentication';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Observable } from 'rxjs';
import { IUser } from "../../model/user/iuser";

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService implements IAuthenticationService, CanActivate {

    constructor(private router: Router, private authentication: Authentication) { }

    public _runAuthorization(user: IUser): boolean {
        let success = false;
        const formData: any = new FormData();
        formData.append('username', user.username);
        formData.append('password', user.password);
        if (user.username == "username" && user.password == "password") {
            success = true;
            this.authentication._setIsLoggedIn(success);
            this.router.navigateByUrl('main-page');

        }
        /*this.httpClient.post(new GlobalVariables()._apiBaseUrl + '/login', formData)
          .subscribe(data => {
            console.log('FINAL RESULT :::::::: ');
            this.authentication._setResponse(data);
            if (this.authentication._getIsLoggedIn()) {
              this.router.navigateByUrl('/index');
              success = true;
            }
          }, (err) => console.log(err))*/
        return success;
    }

    public _stopAuthorization() {
        this.authentication._setIsLoggedIn(false);
        this.router.navigateByUrl('/authentication');
    }
    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean {
        if (this.authentication._getIsLoggedIn()) {
            return true;
        } else {
            this.router.parseUrl('login-page');
            return false;
        }
    }
}
