import {Driver} from "../../driver";
import {IAuthorization} from "./iauthorization";
import {IUser} from "../../model/user/iuser";

export class Authorization implements IAuthorization{

    constructor(private driver: Driver) {

    }

    public _engineLogin(user: IUser): void {
        this.driver._engine.authenticationService._runAuthorization(user);
    }
    public _engineLogout(): void {
        this.driver._engine.authenticationService._stopAuthorization();
    }
}
