import { IUser } from "../../model/user/iuser";

export interface IAuthorization {
    _engineLogin(user: IUser): void;
    _engineLogout(): void;
}
