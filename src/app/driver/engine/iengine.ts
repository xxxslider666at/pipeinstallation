import { IAuthorization } from "./authorization/iauthorization";
import { IAuthenticationService } from './service/iauthentication-service';

export interface IEngine {
    authorization: IAuthorization;
    authenticationService: IAuthenticationService;
}
