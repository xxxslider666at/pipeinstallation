import { IEngine } from "./iengine";
import { Driver } from "../driver";
import { IModel } from "../model/imodel";
import { Authorization } from './authorization/authorization';
import { Authentication } from '../model/authentication/authentication';
import { IAuthenticationService } from "./service/iauthentication-service";
import { IAuthorization } from "./authorization/iauthorization";
import { AuthenticationService } from "./service/authentication.service";


export class Engine implements IEngine {

    constructor(public driver: Driver, private model: IModel, private authentication: Authentication) {
        this._onStartEngine();
        this.authorization = new Authorization(this.driver);
        this.authenticationService = new AuthenticationService(this.driver.router, this.authentication);
    }

    public _onStartEngine(): void {

    }
    public _onInitEngine(): void {

    }

    authorization: IAuthorization;
    authenticationService: IAuthenticationService;
}
