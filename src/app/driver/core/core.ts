import {Driver} from "../driver";
import {ICore} from "./icore";

export class Core implements ICore {
  constructor(public driver: Driver) {
      this._onStartCore();
  }
  public _onStartCore(): void {

  }
  public _onInitCore(): void {}
}
