export interface ICore {
    _onStartCore(): void;
    _onInitCore(): void;
}
