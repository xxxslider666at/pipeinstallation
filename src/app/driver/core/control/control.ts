import {Driver} from "../../driver";

export abstract class Control {
  private controlID: string = "";
  private controlName: string = "";

  protected constructor(public driver: Driver) {}

  public _onSetControlID(controlID: string) {
    this.controlID = controlID;
  }
  public _onGetControlID(): string {
    return this.controlID;
  }
  public _onSetControlName(controlName: string) {
    this.controlName = controlName;
  }
  public _onGetControlName(): string {
    return this.controlName;
  }

  public abstract _onLoadDataInstance(control: Control): void;
  public abstract _onInitInstance(control: Control): void;
  public abstract _onStepBack(control: Control): void;
  public abstract _onPrevious(control: Control): void;
  public abstract _onNext(control: Control): void;
}
