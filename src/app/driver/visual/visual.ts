import {IVisual} from "./ivisual";
import {Driver} from "../driver";

export class Visual implements IVisual {
    constructor(public driver: Driver) {
        this._onStartVisual();
    }

    public _onStartVisual(): void {

    }
    public _onInitVisual(): void {

    }
}
