export interface IVisual {
    _onStartVisual(): void;
    _onInitVisual(): void;
}
