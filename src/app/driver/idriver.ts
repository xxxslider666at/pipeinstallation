import { ICore } from "./core/icore";
import { IEngine } from "./engine/iengine";
import { Router } from "@angular/router";
import {IModel} from "./model/imodel";

export interface IDriver {

    _model: IModel;

    _core: ICore;
    _engine: IEngine;
    _router: Router
}
