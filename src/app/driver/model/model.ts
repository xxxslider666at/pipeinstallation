import { IModel } from "./imodel";
import { IUser } from "./user/iuser";
import { User } from "./user/user";
import { IAuthentication } from "./authentication/iauthentication";
import { Authentication } from "./authentication/authentication";
import { Driver } from "../driver";

export class Model implements IModel {

    public constructor(private driver: Driver) {
        this.user = new User({});
        this.authentication = new Authentication();
    }
    user: IUser;
    authentication: IAuthentication;
}
