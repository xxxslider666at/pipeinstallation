import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit, Directive,
  DoCheck, forwardRef, Inject, Injectable, Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';

import {Component} from "../component/component";
import {Driver} from "../../driver";

export abstract class ViewComponent extends Component implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {
    /*protected constructor(public driver: Driver) {
        super(driver);
    }*/
    ngAfterContentChecked(): void {
    }

    ngAfterContentInit(): void {
    }

    ngAfterViewChecked(): void {
    }

    ngAfterViewInit(): void {
    }

    ngDoCheck(): void {
    }

    ngOnChanges(changes: SimpleChanges): void {
    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
    }
}
