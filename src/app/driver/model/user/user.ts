import {IUser} from "./iuser";

export class User implements IUser {

    public password?: string;
    public username?: string;

    constructor(values: User = {} as IUser) {
        const {
            password = '',
            username = '',
        } = values;

        this.username = username;
        this.password = password;
    }
}
