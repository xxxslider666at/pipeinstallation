import { IUser } from "./user/iuser";
import { IAuthentication } from './authentication/iauthentication';

export interface IModel {
    user: IUser;
    authentication: IAuthentication;
}
