import {IModel} from '../imodel';
import {IDriver} from '../../idriver';

export interface IAuthentication {
  response: any;

  _setResponse(response: any): void;
  _getResponse(): any;

  _setIsLoggedIn(isLoggedIn: boolean): void;
  _getIsLoggedIn(): boolean;
}
