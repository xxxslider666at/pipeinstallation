import { IAuthentication } from './iauthentication';
import { Injectable } from '@angular/core';

@Injectable()
export class Authentication implements IAuthentication {

    constructor() {
    }

    response: any;
    private isLoggedIn = false;

    public _setResponse(response: any): void {
        this.response = response;

        console.log('Response CONTENT ::::::::', this.response);
        console.log('Response CODE ::::::::', this.response.code);
        console.log('Response DATA::ID ::::::::', this.response.data.id);
        console.log('Response DATA::TOKEN ::::::::', this.response.data.token);

        if (this.response.code === 200 && this.response.data.token !== '') {
            this._setIsLoggedIn(true);
            console.log('Is Authenticated ::::::::', true)
            console.log('Authorization is OK !!!');


        } else  {
            console.log('Is Authenticated ::::::::', false);
            console.log('Authorization ERROR !!!');
        }
    }

    public _getResponse(): any {
        return this.response;
    }

    public _setIsLoggedIn(isLoggedIn: boolean): void {
        this.isLoggedIn = isLoggedIn;
    }
    public _getIsLoggedIn(): boolean {
        return this.isLoggedIn;
    }

}
