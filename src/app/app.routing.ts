import { Routes } from '@angular/router';
import { LoginPageComponent } from "./common/page/login-page/login-page.component";
import { WelcomePageComponent } from "./common/page/welcome-page/welcome-page.component";
import { AuthenticationService } from "./driver/engine/service/authentication.service";
import { MainPageComponent } from "./common/page/main-page/main-page.component";
import {RegistrationPageComponent} from "./common/page/registration-page/registration-page.component";


export const AppRoutes: Routes = [
    { path: 'login', redirectTo: 'login-page' },
    { path: 'login-page', component: LoginPageComponent },
    { path: 'main', redirectTo: 'main-page' },
    { path: 'main-page', component: MainPageComponent, canActivate: [AuthenticationService] },
    { path: 'registration', redirectTo: 'registration-page' },
    { path: 'registration-page', component: RegistrationPageComponent },
    { path: '', redirectTo: 'welcome-page', pathMatch: 'full' },
    { path: 'welcome-page', component: WelcomePageComponent }
    /*{ path: 'about-me', redirectTo: 'about-me-page' },
    { path: 'about-me-page', component: AboutMeComponent },
    { path: 'news', redirectTo: 'news-page' },
    { path: 'news-page', component: NewsComponent },
    { path: 'gallery', redirectTo: 'gallery-page' },
    { path: 'gallery-page', component: GalleryComponent }*/
];
