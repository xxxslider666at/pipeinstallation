import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Driver} from "./driver/driver";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "./app.routing";
import { LoginPageComponent } from './common/page/login-page/login-page.component';
import { WelcomePageComponent } from './common/page/welcome-page/welcome-page.component';
import { MainPageComponent } from './common/page/main-page/main-page.component';
import { Authentication } from "./driver/model/authentication/authentication";
import { RegistrationPageComponent } from "./common/page/registration-page/registration-page.component";
import { DisplayComponentComponent } from './common/component/display-component/display-component.component';

@NgModule({
  declarations: [
      AppComponent,
      LoginPageComponent,
      MainPageComponent,
      RegistrationPageComponent,
      WelcomePageComponent,
      DisplayComponentComponent
  ],
  imports: [
      BrowserModule,
      HttpClientModule,
      RouterModule.forRoot(AppRoutes, {enableTracing: true, useHash: true}),
      FormsModule,
      ReactiveFormsModule
  ],
  providers: [Driver, HttpClientModule, HttpClient, Authentication],
  bootstrap: [AppComponent]
})
export class AppModule { }
