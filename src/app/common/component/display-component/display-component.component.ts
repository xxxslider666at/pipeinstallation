import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'display-component',
    templateUrl: './display-component.component.html',
    styleUrls: ['./display-component.component.scss'],
    styles: [`:host {width: 88%; height: 90%}`]
})
export class DisplayComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
