import { Component, OnInit } from '@angular/core';
import { Driver } from "../../../driver/driver";

@Component({
  selector: 'main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

    constructor(public driver: Driver) { }

    ngOnInit(): void {
    }

}
