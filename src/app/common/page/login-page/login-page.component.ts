import { Component, Input, OnInit } from '@angular/core';
import {Driver} from "../../../driver/driver";

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

    @Input() public _inputUsername = '';
    @Input() public _inputPassword = '';

    constructor(private driver: Driver) { }

    ngOnInit(): void {
    }

    public _onClickLogin() {
        this.driver._model.user.username = this._inputUsername;
        this.driver._model.user.password = this._inputPassword;
        this.driver._engine.authorization._engineLogin(this.driver._model.user);
    }

    public _onClickRegistration() {
        this.driver.router.navigateByUrl("registration-page");
    }

}
