import { Component, Input, OnInit } from '@angular/core';
import {Driver} from "../../../driver/driver";

@Component({
  selector: 'registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent implements OnInit {

    @Input() public _inputUsername = '';
    @Input() public _inputPassword = '';

    constructor(private driver: Driver) { }

    ngOnInit(): void {
    }

    public _onClickLogin() {
        this.driver._model.user.username = this._inputUsername;
        this.driver._model.user.password = this._inputPassword;
        this.driver._engine.authorization._engineLogin(this.driver._model.user);
    }

}
