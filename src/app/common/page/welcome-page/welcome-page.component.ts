import { Component, OnInit } from '@angular/core';
import { Driver } from "../../../driver/driver";

@Component({
  selector: 'registration-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.scss']
})
export class WelcomePageComponent implements OnInit {

    constructor(private driver: Driver) { }

    ngOnInit(): void {
    }

    public _onClickLoginAs() {
        this.driver._router.navigateByUrl('login-page');
    }

}
